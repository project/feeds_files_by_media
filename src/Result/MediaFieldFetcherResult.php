<?php

namespace Drupal\feeds_files_by_media\Result;

use Drupal\feeds\Result\FetcherResultInterface;

/**
 * The default fetcher result object.
 */
class MediaFieldFetcherResult implements FetcherResultInterface {
  /**
   * The filepath of the fetched item.
   *
   * @var string
   */
  protected $media_field_machine_name;

  /**
   * Constructs a new FetcherResult object.
   *
   * @param string $media_field_machine_name
   */
  public function __construct($media_field_machine_name) {
    $this->media_field_machine_name = $media_field_machine_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getRaw() {
    return $this->media_field_machine_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilePath() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function cleanUp() {}

}
