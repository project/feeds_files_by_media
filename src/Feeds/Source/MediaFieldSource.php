<?php

namespace Drupal\feeds_files_by_media\Feeds\Source;

use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Item\ItemInterface;
use Drupal\feeds\FeedTypeInterface;
use Drupal\feeds\Plugin\Type\Source\SourceBase;

/**
 * A source plugin that provides feed type fields as mapping sources.
 *
 * @FeedsSource(
 *   id = "file_of_media_field",
 *   category = @Translation("Feeds Files By Media"),
 * )
 */
class MediaFieldSource extends SourceBase {

  /**
   * {@inheritdoc}
   */
  public static function sources(array &$sources, FeedTypeInterface $feed_type, array $definition) {
    $sources['target_file_id_of_media_field'] = [
      // Don't give it an id, then it won't be captured by \Drupal\feeds\EventSubscriber\LazySubscriber::getMappedSourcePlugins.
      'label' => t('Target file id of the media field'),
      'description' => 'Target file id of the media field',
      'type' => 'File of media field',
    ];
    $sources['target_file_name_of_media_field'] = [
      // Don't give it an id, then it won't be captured by \Drupal\feeds\EventSubscriber\LazySubscriber::getMappedSourcePlugins.
      'label' => t('Target file name of the media field'),
      'description' => 'Target file name of the media field',
      'type' => 'File of media field',
    ];
    $sources['target_media_item_name_of_media_field'] = [
      // Don't give it an id, then it won't be captured by \Drupal\feeds\EventSubscriber\LazySubscriber::getMappedSourcePlugins.
      'label' => t('Target media item name of the media field'),
      'description' => 'Target media item name of the media field',
      'type' => 'File of media field',
    ];
    $sources['target_media_item_id_of_media_field'] = [
      // Don't give it an id, then it won't be captured by \Drupal\feeds\EventSubscriber\LazySubscriber::getMappedSourcePlugins.
      'label' => t('Target media item id of the media field'),
      'description' => 'Target media item id of the media field',
      'type' => 'File of media field',
    ];
    $sources['target_media_item_uuid_of_media_field'] = [
      // Don't give it an id, then it won't be captured by \Drupal\feeds\EventSubscriber\LazySubscriber::getMappedSourcePlugins.
      'label' => t('Target media item uuid of the media field'),
      'description' => 'Target media item uuid of the media field',
      'type' => 'File of media field',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceElement(FeedInterface $feed, ItemInterface $item) {
    return $item;
  }

}
