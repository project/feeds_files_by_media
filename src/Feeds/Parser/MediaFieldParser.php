<?php

namespace Drupal\feeds_files_by_media\Feeds\Parser;

use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Item\DynamicItem;
use Drupal\feeds\Feeds\Parser\ParserBase;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\StateInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * Defines a media fields parser.
 *
 * @FeedsParser(
 *   id = "media_field",
 *   title = "Parser Media Field",
 *   description = @Translation("Parser for fetch:'Fetch Resource from media
 *   field'."),
 * )
 */
class MediaFieldParser extends ParserBase {

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state) {
    $media_field_id = $fetcher_result->getRaw();
    $media_value = $this->getMediaFieldData($feed, $media_field_id);

    $result = new ParserResult();
    foreach ($media_value as $row) {
      $item = new DynamicItem();
      /** @var \Drupal\media\Entity\Media $media */
      $media = Media::load($row['target_id']);
      $file_id = $media->get('field_media_image')->getValue();
      $file_id = $file_id[0]['target_id'];
      $file = File::load($file_id);
      $item->set('target_file_id_of_media_field', $file_id);
      $item->set('target_file_name_of_media_field', $file->getFilename());
      $item->set('target_media_item_name_of_media_field', $media->getName());
      $item->set('target_media_item_id_of_media_field', $media->id());
      $item->set('target_media_item_uuid_of_media_field', $media->uuid());

      $result->addItem($item);
    }

    // Report progress.
    $files_count = count($media_value);
    $state->total = $files_count;
    $state->pointer = $files_count;
    $state->progress($state->total, $state->pointer);

    if (!$result->count()) {
      $state->setCompleted();
    }

    return $result;
  }

  /**
   * @param FeedInterface $feed
   *
   * @return  array
   */
  public function getMediaFieldData($feed, $media_field_id) {
    return $feed->get($media_field_id)->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingSources() {
    return [];
  }

}
