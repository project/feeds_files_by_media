<?php

namespace Drupal\feeds_files_by_media\Feeds\Fetcher;

use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\StateInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds_files_by_media\Result\MediaFieldFetcherResult;

/**
 * Fetches data using plupload form element.
 *
 * @FeedsFetcher(
 *   id = "media_field",
 *   title = @Translation("Fetch Resource from media field"),
 *   description = @Translation("Upload files by feed's media field."),
 *   form = {
 *     "configuration" = "Drupal\feeds_files_by_media\Feeds\Fetcher\Form\MediaFieldFetcherConfigForm",
 *   },
 * )
 */
class MediaFieldFetcher extends PluginBase implements FetcherInterface {

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $media_field_machine_name = $this->getConfiguration('media_field_machine_name');
    return new MediaFieldFetcherResult($media_field_machine_name);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'media_field_machine_name' => '',
    ];
  }

}
