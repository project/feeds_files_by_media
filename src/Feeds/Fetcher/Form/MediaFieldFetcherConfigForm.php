<?php

namespace Drupal\feeds_files_by_media\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

/**
 * The configuration form for the plupload fetcher.
 */
class MediaFieldFetcherConfigForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['media_field_machine_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Machine name of the media field"),
      '#description' => $this->t('Machine name of the media field in this
       feeds type which will be used for the fetched resource.'),
      '#default_value' => $this->plugin->getConfiguration('media_field_machine_name'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo validate media field.
    // If no there's no this media field name on this feeds type, then give
    // notice.
  }

}
