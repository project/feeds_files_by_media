## Feeds files by media

Using feeds to import files to entity by media field.

## How to use
1. Install the module by composer
2. Enable the module
3. Add an feeds type: 
    1. Select the fetcher: Fetch Resource from media field.
    2. Select the parser: Media field parser
    3. Setting the "Machine name of the media field" in fetcher settings.
4. Add a media type field whose machine name must use the "machine name" set in fetcher settings of above step.
5. Go to mapping settings tab of the feeds type
   1. Create mapping using the source of "Target file id of the media field" to mapping the image/file field of the entity.
   2. Click above mapping configure setting, then select "File Id" of the "Reference by"